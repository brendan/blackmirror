<!-- .slide: data-background-video="img/background.mp4" -->
<!-- .slide: data-background-video-loop="true" -->

---

## Black Mirror Season 5:
## DevOps

<br/><br/<br/>

Brendan O'Leary

Implementation Engineer @ <i class="fab fa-gitlab gitlab-orange-text"></i>

Note:
Introduction
How many have heard of / seen black mirror on Netflix?

---
<!-- .slide: data-background-image="img/blackmirror169.png" -->

Note:
If you haven’t, highly recommend


Just not right before you want to go to sleep

* Examines technology taken to the extreme and the negative impacts they can have.  
* At its core it’s about the mistakes humans have made and continue to make throughout history 

* Our screens that when they are off show us a black image of ourselves - 
* to comment on some of our innate flaws which will be exacerbated in technology if we aren’t careful

---
<!-- .slide: data-background-image="img/nosedive.jpg" -->
<!-- .slide: class="top-bg-img" -->
<!-- .slide: data-autoslide="2500" -->

Note:
* I swear I'll tie this into my opnion on DevOps, but first let's take a little journey though Black Mirror's universe
* Nosedive
* Social media/ranking leads to loss of our humanity
* Chasing likes doesn't align us to the values that make for a good person 

---
<!-- .slide: data-background-image="img/nosedive3.jpg" -->
<!-- .slide: data-autoslide="2000" -->

Note:
* Nosedive
* Social media/ranking leads to loss of our humanity
* Chasing likes doesn't align us to the values that make for a good person 

---
<!-- .slide: data-background-image="img/nosedive2.jpg" -->
<!-- .slide: data-autoslide="2500" -->

Note:
* Nosedive
* Social media/ranking leads to loss of our humanity
* Chasing likes doesn't align us to the values that make for a good person 

---
<!-- .slide: data-background-video="img/bg-nosedive.mp4" -->
<!-- .slide: data-background-video-loop="true" -->

Note:
* Nosedive
* Social media/ranking leads to loss of our humanity
* Chasing likes doesn't align us to the values that make for a good person 

---
<!-- .slide: data-background-image="img/hatedinthenation4.png" -->
<!-- .slide: data-autoslide="3000" -->

Note:
* Hated in the nation
* Automation for automation’s sake can be weaponized
* We often arrgoantly think we have complete control over technology
* We also love to anynomously villify others

---
<!-- .slide: data-background-image="img/hatedinthenation1.jpg" -->
<!-- .slide: data-autoslide="2500" -->

Note:
* Hated in the nation
* Automation for automation’s sake can be weaponized
* We often arrgoantly think we have complete control over technology
* We also love to anynomously villify others

---
<!-- .slide: data-background-image="img/hatedinthenation3.jpg" -->
<!-- .slide: data-autoslide="2000" -->

Note:
* Hated in the nation
* Automation for automation’s sake can be weaponized
* We often arrgoantly think we have complete control over technology
* We also love to anynomously villify others

---
<!-- .slide: data-background-video="img/bg-hatedinthenation.mp4" -->
<!-- .slide: data-background-video-loop="true" -->

Note:
* Hated in the nation
* Automation for automation’s sake can be weaponized
* We often arrgoantly think we have complete control over technology
* We also love to anynomously villify others

---
<!-- .slide: data-background-image="img/whitebear2.jpg" -->
<!-- .slide: data-autoslide="2000" -->

Note:
* Institutionalizing parts of society as “bad” is bad bad
* White bear
* Men against fire
* White christmas

---
<!-- .slide: data-background-image="img/whitebear3.jpg" -->
<!-- .slide: data-autoslide="1500" -->

Note:
* Institutionalizing parts of society as “bad” is bad bad
* White bear
* Men against fire
* White christmas

---
<!-- .slide: data-background-image="img/menagainstfire3.jpg" -->
<!-- .slide: class="top-bg-img" -->
<!-- .slide: data-autoslide="1500" -->

Note:
* Institutionalizing parts of society as “bad” is bad bad
* White bear
* Men against fire
* White christmas

---
<!-- .slide: data-background-image="img/menagainstfire2.jpg" -->
<!-- .slide: data-autoslide="1500" -->

Note:
* Institutionalizing parts of society as “bad” is bad bad
* White bear
* Men against fire
* White christmas

---
<!-- .slide: data-background-image="img/whitechristmas1.jpg" -->
<!-- .slide: data-autoslide="1500" -->

Note:
* Institutionalizing parts of society as “bad” is bad bad
* White bear
* Men against fire
* White christmas

---
<!-- .slide: data-background-image="img/menagainstfire.png" -->
<!-- .slide: data-autoslide="3000" -->

Note:
* Institutionalizing parts of society as “bad” is bad bad
* White bear
* Men against fire
* White christmas

---
<!-- .slide: data-background-video="img/bg-menagainstfire.mp4" -->
<!-- .slide: data-background-video-loop="true" -->
<!-- .slide: class="top-bg-img" -->

Note:
* Institutionalizing parts of society as “bad” is bad bad
* White bear
* Men against fire
* White christmas
* (if past 3 mins, you're good)


---
<!-- .slide: data-background-image="img/kelly-sikkema-189822.jpg" -->

## Season 5
## DevOps

Note:
* Seem like horror stories - but also seem just moments away from the world we are in today.  Real stories show us similar issues in the news.  
* Applying technology in the right way
* What about in our world.  What about in DevOps?
* Let's talk about the first 3 episodes of Season 5...that have already been released.

---
## Epsiode 1
#### Equifax Breach

![Equifax](img/equifax.png)

Note:
* 146 million people (including me)
* Apache Struts 
* Patched withing 3 days
* Left in Equifax prdo for over a year
* admin/admin for Argentinan site

---

## Epsiode 2

#### Meltdown and Spectre

![Meltdown and Spectre](img/meltdownandspectre.png)

Note:
* No matter what your plans are around security, meltdown and spectre made us all rethink security
* I'm assuming many of us saw the presentation yesterday from Barbara?
* If you didn't a quick summary of Meltdown/Spectre
* Steal data from other applications in memory...the one place you don't worry about in programming

---

#### Meltdown and Spectre

![Meltdown and Spectre](img/meltdown_and_spectre.png)

Note:
* Do we just suck at...computers?
* Yup.  Especially shared ones

---

## Episode 3

#### Knight Capital

![Knight Capital](img/knightcaptial.png)

Note:
* $440 million - Almost 2x their quarterly revenue ($10 million a minute)
* "The company said the problems happened because of new trading software that had been installed."
* "During the deployment of the new code, however, one of Knight’s technicians did not copy the new code to one of the eight SMARS computer servers."
* They were unable to determine what was causing the erroneous orders they reacted by uninstalling the new code from the servers it was deployed to correctly. In other words, they removed the working code and left the broken code. 

---
<!-- .slide: data-background-image="img/piotr-chrobot-278530.jpg" -->
<!-- .slide: class="invert-slide" -->

## Episode 4

#### The Illusion of Safety

Note:
PAUSE

We open on children playing on the play ground.  Their father laughs as they run around in circles.  Suddenly his phone rings.  He sees who it is and immediatly his face goes sour.  He thinks for a minute and then lowers the phone.  As soon as he's done that, it starts ringing again.  He picks it up and as he answers it everything around him fades to a blurry black.  He's not in the park at all - he's in the office all weekend.  He's been trying to figure out since early Friday which security patches apply to which parts of his infrastructure.  They had a security plan with alerts.  But they've all failed him - and he doesn't want to be in front of Congress explaining why they didn't patch a known issue for 2 years.

---
<!-- .slide: data-background-image="img/daniel-ruyter-53648-unsplash.jpg" -->

Note:
We open on children playing on the play ground.  Their father laughs as they run around in circles.  Suddenly his phone rings.  He sees who it is and immediatly his face goes sour.  He thinks for a minute and then lowers the phone.  As soon as he's done that, it starts ringing again.  He picks it up and as he answers it everything around him fades to a blurry black.  He's not in the park at all - he's in the office all weekend.  He's been trying to figure out since early Friday which security patches apply to which parts of his infrastructure.  They had a security plan with alerts.  But they've all failed him - and he doesn't want to be in front of Congress explaining why they didn't patch a known issue for 2 years.

---
<!-- .slide: data-background-image="img/kelly-sikkema-592857-unsplash.jpg" -->
<!-- .slide: class="top-bg-img" -->

Note:
We open on children playing on the play ground.  Their father laughs as they run around in circles.  Suddenly his phone rings.  He sees who it is and immediatly his face goes sour.  He thinks for a minute and then lowers the phone.  As soon as he's done that, it starts ringing again.  He picks it up and as he answers it everything around him fades to a blurry black.  He's not in the park at all - he's in the office all weekend.  He's been trying to figure out since early Friday which security patches apply to which parts of his infrastructure.  They had a security plan with alerts.  But they've all failed him - and he doesn't want to be in front of Congress explaining why they didn't patch a known issue for 2 years.

---
<!-- .slide: data-background-image="img/martino-pietropoli-584872-unsplash.jpg" -->
<!-- .slide: class="top-bg-img" -->

Note:
We open on children playing on the play ground.  Their father laughs as they run around in circles.  Suddenly his phone rings.  He sees who it is and immediatly his face goes sour.  He thinks for a minute and then lowers the phone.  As soon as he's done that, it starts ringing again.  He picks it up and as he answers it everything around him fades to a blurry black.  He's not in the park at all - he's in the office all weekend.  He's been trying to figure out since early Friday which security patches apply to which parts of his infrastructure.  They had a security plan with alerts.  But they've all failed him - and he doesn't want to be in front of Congress explaining why they didn't patch a known issue for 2 years.

---
<!-- .slide: data-background-image="img/kelly-sikkema-102446-unsplash.jpg" -->

Note:
We open on children playing on the play ground.  Their father laughs as they run around in circles.  Suddenly his phone rings.  He sees who it is and immediatly his face goes sour.  He thinks for a minute and then lowers the phone.  As soon as he's done that, it starts ringing again.  He picks it up and as he answers it everything around him fades to a blurry black.  He's not in the park at all - he's in the office all weekend.  He's been trying to figure out since early Friday which security patches apply to which parts of his infrastructure.  They had a security plan with alerts.  But they've all failed him - and he doesn't want to be in front of Congress explaining why they didn't patch a known issue for 2 years.


---
<!-- .slide: data-background-image="img/david-werbrouck-304966.jpg" -->
<!-- .slide: class="invert-slide" -->

## Episode 5

#### Sisyphean Deployment

Note:
PAUSE 

The fixes and features a customer wants are done.  They've been done for weeks.  But we keep going back over them in meeting after meeting: a groundhog day of hell where no one can figure out how to bring it all together into one release and get it out the door.  Every morning the alarm clock goes off and Molly grunts disapprovingly.  She slowly starts crying to no one at all as she knows this day will not bring relief from integration, release and "scrum" meetings that go on for hours.

---
<!-- .slide: data-background-image="img/kelly-sikkema-284964-unsplash.jpg" -->

Note:
The fixes and features a custoemr wants are done.  They've been done for weeks.  But we keep going back over them in meeting after meeting: a groundhog day of hell where no one can figure out how to bring it all together into one release and get it out the door.  Every morning the alarm clock goes off and Molly grunts disapprovingly.  She slowly starts crying to no one at all as she knows this day will not bring relief from integration, release and "scrum" meetings that go on for hours.

---
<!-- .slide: data-background-image="img/charles-deluvio-575493-unsplash.jpg" -->

Note:
The fixes and features a custoemr wants are done.  They've been done for weeks.  But we keep going back over them in meeting after meeting: a groundhog day of hell where no one can figure out how to bring it all together into one release and get it out the door.  Every morning the alarm clock goes off and Molly grunts disapprovingly.  She slowly starts crying to no one at all as she knows this day will not bring relief from integration, release and "scrum" meetings that go on for hours.

---
<!-- .slide: data-background-image="img/adam-birkett-486736-unsplash.jpg" -->

Note:
The fixes and features a custoemr wants are done.  They've been done for weeks.  But we keep going back over them in meeting after meeting: a groundhog day of hell where no one can figure out how to bring it all together into one release and get it out the door.  Every morning the alarm clock goes off and Molly grunts disapprovingly.  She slowly starts crying to no one at all as she knows this day will not bring relief from integration, release and "scrum" meetings that go on for hours.

---
<!-- .slide: data-background-image="img/charles-deluvio-575493-unsplash.jpg" -->

Note:
The fixes and features a custoemr wants are done.  They've been done for weeks.  But we keep going back over them in meeting after meeting: a groundhog day of hell where no one can figure out how to bring it all together into one release and get it out the door.  Every morning the alarm clock goes off and Molly grunts disapprovingly.  She slowly starts crying to no one at all as she knows this day will not bring relief from integration, release and "scrum" meetings that go on for hours.

---
<!-- .slide: data-background-image="img/samuel-zeller-4138-unsplash.jpg" -->

Note:
The fixes and features a custoemr wants are done.  They've been done for weeks.  But we keep going back over them in meeting after meeting: a groundhog day of hell where no one can figure out how to bring it all together into one release and get it out the door.  Every morning the alarm clock goes off and Molly grunts disapprovingly.  She slowly starts crying to no one at all as she knows this day will not bring relief from integration, release and "scrum" meetings that go on for hours.

---
<!-- .slide: data-background-image="img/charles-deluvio-575493-unsplash.jpg" -->

Note:
The fixes and features a custoemr wants are done.  They've been done for weeks.  But we keep going back over them in meeting after meeting: a groundhog day of hell where no one can figure out how to bring it all together into one release and get it out the door.  Every morning the alarm clock goes off and Molly grunts disapprovingly.  She slowly starts crying to no one at all as she knows this day will not bring relief from integration, release and "scrum" meetings that go on for hours.


---
<!-- .slide: data-background-image="img/joey-nicotra-510944-unsplash.jpg" -->
<!-- .slide: class="invert-slide" -->

## Episode 6

#### Super Hero or Tax Collector?

Note:
PAUSE

Ned is awake before his alarm.  The need for his powers can awaken him from even the deepest slumber.  Somewhere in the world, some one needs him.  He puts on his bright red cape and the rest of his suit.  At work, everyone knows he's a super hero.  He always has a new tool for any problem that comes up.

But in reality, that's not how his peers see him.  They see DevOps as a cost center, and Ned as the tax collector.  For each new problem, his solution is always another tool.  More money and time spent integrationg them, and longer time to deliver software.

---
<!-- .slide: data-background-image="img/colin-rex-413679-unsplash.jpg" -->

Note:
Ned is awake before his alarm.  The need for his powers can awaken him from even the deepest slumber.  Somewhere in the world, some one needs him.  He puts on his bright red cape and the rest of his suit.  At work, everyone knows he's a super hero.  He always has a new tool for any problem that comes up.

But in reality, that's not how his peers see him.  They see DevOps as a cost center, and Ned as the tax collector.  For each new problem, his solution is always another tool.  More money and time spent integrationg them, and longer time to deliver software.

---
<!-- .slide: data-background-image="img/zhen-hu-517739-unsplash.jpg" -->

Note:
Ned is awake before his alarm.  The need for his powers can awaken him from even the deepest slumber.  Somewhere in the world, some one needs him.  He puts on his bright red cape and the rest of his suit.  At work, everyone knows he's a super hero.  He always has a new tool for any problem that comes up.

But in reality, that's not how his peers see him.  They see DevOps as a cost center, and Ned as the tax collector.  For each new problem, his solution is always another tool.  More money and time spent integrationg them, and longer time to deliver software.

---
<!-- .slide: data-background-image="img/lena-kohler-388633.jpg" -->

Note:
Ned is awake before his alarm.  The need for his powers can awaken him from even the deepest slumber.  Somewhere in the world, some one needs him.  He puts on his bright red cape and the rest of his suit.  At work, everyone knows he's a super hero.  He always has a new tool for any problem that comes up.

But in reality, that's not how his peers see him.  They see DevOps as a cost center, and Ned as the tax collector.  For each new problem, his solution is always another tool.  More money and time spent integrationg them, and longer time to deliver software.

---
<!-- .slide: data-background-image="img/lena-orwig-491551-unsplash.jpg" -->

Note:
Ned is awake before his alarm.  The need for his powers can awaken him from even the deepest slumber.  Somewhere in the world, some one needs him.  He puts on his bright red cape and the rest of his suit.  At work, everyone knows he's a super hero.  He always has a new tool for any problem that comes up.

But in reality, that's not how his peers see him.  They see DevOps as a cost center, and Ned as the tax collector.  For each new problem, his solution is always another tool.  More money and time spent integrationg them, and longer time to deliver software.

---
<!-- .slide: data-background-image="img/freddie-collins-309833-unsplash.jpg" -->

Note:
Ned is awake before his alarm.  The need for his powers can awaken him from even the deepest slumber.  Somewhere in the world, some one needs him.  He puts on his bright red cape and the rest of his suit.  At work, everyone knows he's a super hero.  He always has a new tool for any problem that comes up.

But in reality, that's not how his peers see him.  They see DevOps as a cost center, and Ned as the tax collector.  For each new problem, his solution is always another tool.  More money and time spent integrationg them, and longer time to deliver software.

---

<h3>It's not too late</h3>
<h3 class="fragment">It doesn't have to be this way</h3>
<h5 class="fragment" style="color: #FC9403;">With great power comes great responsiblity </h5>

---
<!-- .slide: data-background-image="img/dardan-mu-268793.jpg" -->

Note:
* There are solutions to all of these problems.  We don't have to let these problems become our future.
* We've been given a gift today.  An glimpse of what could be.  A chance to make a better future for us, our development collegues and our customers. 

---
<!-- .slide: data-background-image="img/freddie-collins-309833-unsplash.jpg" -->
<!-- .slide: class="invert-slide" -->
## Avoid the DevOps Tax 💸

Note:
* All of these items boil up to avoiding the DevOps Tax
* Be seen as a strategic advantage by the business, not a cost center constantly asking for new toys

---
<!-- .slide: data-background-image="img/scott-webb-403356-unsplash.jpg" -->
<!-- .slide: class="invert-slide" -->

## Be the change you seek 🦊
  - Reduce cycle time
  - Minimum Viable Change (MVC)
  - Lead the charge

Note:
* In order to do that, I think the most crtical thing is for us to take responsiblity for these changes in viewpoint

---
<!-- .slide: data-background-image="img/dardan-mu-268793.jpg" -->
<!-- .slide: class="invert-slide" -->

### What can you do TODAY?

* **Don't** interface ➡️ **do** integrate
* **Do** add more value than "automated builds" 🤖
  - Provide self-service to development staff 🤳
* **Do** Monitor all the things 📊

Note:
* Don't interface with things like security.  Integrate them into the pipeline
* Don't let the org think DevOps is just automated builds
* Provide self-service. That will lead to Developer Driven Pipelines
* Do orchestrate monitoring to complete the feedback loop

---
## One with security

![One with the force](img/onewiththeforce.png)

Note:
My wife told me I couldn't present at a 'tech conference' without a Star Wars reference. 

---
## Shift Left

- Move security left in the pipeline <!-- .element: class="fragment" -->
- Should not be an after thought <!-- .element: class="fragment" -->
- MUST to be automated <!-- .element: class="fragment" -->

<p class="fragment">
Do not make it a burden on teams, <br/> 
but an *enabler* of speed
</p>

<h3 class="fragment">High performing teams <span style="color: #fc6d26;">git</span> it</h3>

- 50% less time remediating security issues <!-- .element: class="fragment" -->
- 27% more automated tests <!-- .element: class="fragment" -->

Note:
And a Git joke for good measure

---

<!-- .slide: data-background-size="80%" -->

<img src="img/chart2.png" width="5000" />

<small>State of Application Security, 2018</small>

Note:
* Survey of security professionals who had experienced a penetration attack.
* How was the attack carried out?


---

# What should Equifax look like?

Note:
* In a truely automated SDLC, it is much simpler
* Instead of a manual alert to be missed, its fully automated
* CVE -> patch -> auto MR -> testing -> prod

---
<!-- .slide: data-background-image="img/kevin-364843.jpg" -->
<!-- .slide: class="invert-slide" -->

## Automated builds are NOT CI/CD

---

Automated builds ≠ CI/CD

Automated builds != CI/CD

Automated builds =/= CI/CD

Automated builds <> CI/CD

Automated builds `.NE.` CI/CD

Automated builds `'=` CI/CD

Note:
* Translated this to other languages to help out...
* Continuous is often lost
* Integrate code early and often
* Care about tests, care about tests passing
* D - delivery or deployment
* Automated packaging, releasing 
* Automated deployment is required to do CD

---
### Data is the new testing

What is <strike>measured</strike> monitored can be improved

![So Fetch](img/so-fetch.gif)

---
### Data is the new testing

![Graph](img/fleetload.png)

Note:
* SDLC is a life cycle - it doesn't wnet
* Monitoring must be part of the plan from the begining 
* Canary, ship then iterate, MVC

---
#### DevOps and "traditional" industries

<table>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Transporation
		</td>
		<td>
			Tesla; Ridesharing
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Healthcare
		</td>
		<td>
			Amazon; CVS
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Groceries
		</td>
		<td>
			Amazon
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Things
		</td>
		<td>
			IoT
		</td>
	</tr>
	<tr class="fragment fade-up">
		<td class="gitlab-orange-text">
			Your Industry
		</td>
		<td>
			Despite what anyone says
		</td>
	</tr>
</table>

Note:
* If you think "that's great for startups, but not in my industry"
* There's probably actually some Amazon industries I haven't thought of - others?
* A lot finanical services companies, energy companies

---

<img src="img/amazoneffect.png" width="850"/>

<small>
Source: https://www.bloomberg.com/graphics/2018-amazon-industry-displacement/
</small>

---
#### DevOps and "traditional" industries

<table>
	<tr>
		<td class="gitlab-orange-text">
			Transporation
		</td>
		<td>
			Tesla; Ridesharing
		</td>
	</tr>
	<tr>
		<td class="gitlab-orange-text">
			Healthcare
		</td>
		<td>
			Amazon; CVS
		</td>
	</tr>
	<tr>
		<td class="gitlab-orange-text">
			Groceries
		</td>
		<td>
			Amazon
		</td>
	</tr>
	<tr>
		<td class="gitlab-orange-text">
			Things
		</td>
		<td>
			IoT
		</td>
	</tr>
	<tr class="fragment grow">
		<td class="gitlab-orange-text">
			Your Industry
		</td>
		<td>
			Despite what anyone says
		</td>
	</tr>
</table>

Note:
* If you think "Brendan my boss doesn't get it"
* That's not their problem - its yours
* We are the DevOps professionals...we need to own it

---
### Everyone can contribute

![Mario Tanuki](img/MarioTanuki.jpg)

Note:
- Stop building silos, build bridges
- Place customer value first
- Don't let perfect get in the way of good..or even shipped

---

![Black Mirror Tweet](img/tweet.png)


---

As Kanye West said:

> [We're] living in the future so

> the present is [our] past.

Note:
* I could talk forever about DevOps.  Feel free to find me at the conference
* But if you take one thing away from this talk it should be
* Shift Left - Control your own destiny by controling the process end to tend
* When you interact with your boss, your bosses boss, your colleuges, your peers
* (next slide)

---

### TFW you Shift Left

<img src="img/gitlabfan-smile.png" class="plain">

Note:
* You can choose to use DevOps powers for good.  
* And when we do we will build a better future for ourselves, for our teams and for our world
* Thank you

---

#### https://boleary.gitlab.io/blackmirror/
<i class="fab fa-twitter" style="color: #00aced;"></i> 
<a href="https://twitter.com/olearycrew">olearycrew</a>
&nbsp;&nbsp;&nbsp;&nbsp;
<i class="fab fa-gitlab gitlab-orange-text"></i> 
<a href="https://gitlab.com/boleary">boleary</a>

<img src="img/gitlabfan-smile.png" class="plain">

---

<small>
This is literally just a slide of gifs.
</small>

<table>
	<tr>
		<td align="center">
			<img src="img/gifs/1.gif" height="125" />
		</td>
		<td align="center">
			<img src="img/gifs/2.gif" height="125"/>
		</td>
		<td align="center">
			<img src="img/gifs/3.gif" height="125"/>
		</td>
	</tr>
	<tr>
		<td align="center">
			<img src="img/gifs/4.gif" height="125"/>
		</td>
		<td align="center">
			<img src="img/gifs/5.gif" height="125"/>
		</td>
		<td align="center">
			<img src="img/gifs/6.webp" height="125" />
		</td>
	</tr>
	<tr>
		<td align="center">
			<img src="img/gifs/7.gif" height="125"/>
		</td>
		<td align="center">
			<img src="img/gifs/8.webp" height="125" />
		</td>
		<td align="center">
			<img src="img/gifs/9.gif" height="125"/>
		</td>
	</tr>
</table>

---

Photo & Video Credits

<small>


- Netflix
- Photo by Kelly Sikkema on Unsplach
- Photo by Lukas Juhas on Unsplash
- Photo by Piotr Chrobot on Unsplash
- Photo by David Werbrouck on Unsplash
- Photo by Lena Köhler on Unsplash
- Photo by Kevin on Unsplash
- Photo by Daniel Ruyter on Unsplash
- Photo by Kelly Sikkema on Unsplash
- Photo by Martino Pietropoli on Unsplash
- Photo by Adam Birkett on Unsplash
- Photo by Sweet Ice Cream Photography on Unsplash
- Photo by Scott Webb on Unsplash
- Photo by Charles Deluvio 🇵🇭🇨🇦 on Unsplash
- Photo by Samuel Zeller on Unsplash
- Some Google Images...
- Giphy !!!


</small>